<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="Devendra Shukla">
      <meta name="generator" content="Jekyll v3.8.5">
      <title>DAStek Access Manager</title>
      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- Custom styles for this template -->
      <link href="css/signin.css" rel="stylesheet"> 
      <link href="css/main.css" rel="stylesheet">
   </head>
   <body class="text-center">
      <!---logout call--->
      <cfif StructKeyExists(URL,"logout")>
         <cfset createobject("component",'AccessManager.authenticationService').doLogout()>
      </cfif>
      <!---logout end--->  
      <div class="login-html">
            <!---Form Processiong begin here---->
            <cfif structkeyExists(form,'fld_submitLogin')>
               <!---create an instance of the authentication service --->
               <cfset authenticationService = createobject("component",'AccessManager.authenticationService')/>     
               <!-- Server side data validation -->
               <cfset aErrorMessages =  authenticationService.validateUser(form.fld_userEmail,form.fld_userPassword) />
               <cfif ArrayisEmpty(aErrorMessages)>
                  <!---- proceed to the login procedure ----->
                  <cfset isUserLoggedIn =  authenticationService.doLogin(form.fld_userEmail,form.fld_userPassword) />
                  
                  <cfif isUserLoggedIn >
                     <cfif structKeyExists(session,'stUserLogin')></cfif>
                        <cflocation url="homePage.cfm" addtoken="no">
                        <cfelse>
                        <strong>Please Enter Valid Ceredentials</strong>
                     </cfif>
                  </cfif>
               </cfif>
            <!---Form Processing end here--->
            <form class="form-signin" id="frmConnexion" preservedata="true" method="post">
               <h1 class="loginH1">DAStek Access Manager</h1>
               <img class="mb-4 profileImage" src="css/images/img_avatar.png" alt="">
               <br><br/>
               <label for="fld_userEmail" class="sr-only">Email address</label>
               <input type="email" name="fld_userEmail" id="fld_userEmail" required="true"  validate="email" validateAt="onSubmit"class="form-control" placeholder="Email address" ><br/>
               <label for="fld_userPassword" class="sr-only">Password</label>
               <input type="password" name="fld_userPassword" id="fld_userPassword"   required="true"  validateAt="onSubmit" class="form-control" placeholder="Password" >
               
               <input type="submit" name="fld_submitLogin" id="fld_submitLogin"class="btn btn-primary btn-lg active btnn" value="Login" >
               <br><br>
               <span><a class="a1" href="forgotPassword.cfm"> Forgot Password?</a></span>
               <br>
               <p class="loginP">
               <cfset yearNow = #Year(now())# />
                <cfoutput>
                    &copy; Copyright #yearNow# <a class="a1" href="http://www.dasteksoftwares.com/" target="_blank">DAStek Softwares</a>
                </cfoutput>
               </p>
            </form>
         </div>
      </div>
   </body>
</html>