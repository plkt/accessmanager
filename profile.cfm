<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <title>DAStek ACCESS MANAGER</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href="css/signin.css" rel="stylesheet">
            <link href="css/main.css" rel="stylesheet">
        </head>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
        <body>
            <cfset userObj = createobject("component",'user')/>
            <cfset getallUsersData=userObj.retrieveUsersData(SESSION.stUserLogin.userId)/>

            <cfif StructKeyExists(FORM,'btn_Update')>
                
                <cfset qUpdateProfile=userObj.updateProfile(FORM)/>
        
            </cfif>
            <!-- NAVIGATION CODE START FROM HERE  -->
            <cfinclude template="includes/header.cfm">
            <!-- NAVIGATION END -->
            <div class="container-fluid">
                <div class="row">
                    <!-- SIDE NAVIGATION -->
                    <cfinclude template="includes/sideNav.cfm">
                    <!-- SIDE NAVIGATION END-->
                    <!-- MAINPAGE CODE -->
                    <div class="col-md-1 mainpage" ></div>
                    <div class="col-md-6 mainpage" >
                        <h2><b>Profile</b></h2><hr>
                        <form class="form-horizontal" method="Post">
                        <cfoutput>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="uname">NAME:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="uname" placeholder=" User's Name" name="uname" Value="#getallUsersData.user_name#">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">EMAIL:</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" id="email" placeholder=" email" name="email" Value="#getallUsersData.user_email#">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="contactno">CONTACT NO.</label>
                                <div class="col-sm-5">          
                                    <input type="text" class="form-control" id="contactno" placeholder=" Contact No." name="contactno" Value="#getallUsersData.contact_no#">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="contactno">PASSWORD:</label>
                                <cfset decryptedPass=decrypt(getallUsersData.user_password,'12345')>
                                <div class="col-sm-5">          
                                    <input type="password" class="form-control" id="pass" placeholder="Password" name="pass" Value="#decryptedPass#">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <input type="submit" class="btn btn-primary btn-lg active" Value="Update" name="btn_update"></a>
                                </div>
                            </div>
                        </cfoutput>
                        </form>
                    </div>
                    <!-- MAINPAGE CODE END -->	
                </div>
            </div>
            <cfinclude template="includes/footer.cfm">
        </body>
    </html>

    <cfelse>
           
    <!DOCTYPE html>
    <html lang="en">
        <head>
        <title>DAStek ACCESS MANAGER</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href="css/signin.css" rel="stylesheet">
            <link href="css/main.css" rel="stylesheet">
        </head>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
        <body >
            <cfset projectObj = createobject("component",'project')/>
            <cfset getallProjects = projectObj.specificUserProjects(session.stUserLogin.userId)/>

            <cfset userObj = createobject("component",'user')/>
            <cfset getallUsersData=userObj.retrieveUsersData(SESSION.stUserLogin.userId)/>

            <cfif StructKeyExists(FORM,'btn_Update')>
               
                <cfset qUpdateProfile=userObj.updateProfile(FORM)/>
                
            </cfif>
            <!--- for showing details of project to user--->
            <!---<cfif StructKeyExists(FORM,'action') AND FORM.action EQ 'Submit'>
            <cfset qpoppulateProj=projectObj.retrieveProjectsData(FORM.myproject)/>
            </cfif>--->
            <!-- NAVIGATION CODE START FROM HERE  -->
            <cfinclude template="includes/header.cfm">
            <!-- NAVIGATION END -->
            <div class="container-fluid">
                <div class="row">
                
                    <!-- SIDE NAVIGATION END-->
                    <!-- MAINPAGE CODE -->
                    <div class="col-md-4 mainpage" ></div>
                    <div class="col-md-6 mainpage" >
                        <h2><b>Profile</b></h2><hr>
                        <cfoutput>
                            <form class="form-horizontal"  method="POST">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="uname">NAME:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="uname" placeholder=" User's Name" name="uname" Value="#getallUsersData.user_name#">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="email">EMAIL:</label>
                                    <div class="col-sm-5">
                                        <input type="email" class="form-control" id="email" placeholder=" email" name="email" Value="#getallUsersData.user_email#">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="pwd">CONTACT NO.</label>
                                    <div class="col-sm-5">          
                                        <input type="text" class="form-control" id="contactno" placeholder="Contact No." name="contactno" Value="#getallUsersData.contact_no#">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="contactno">PASSWORD:</label>
                                    <cfset decryptedPass=decrypt(getallUsersData.user_password,'12345')>
                                    <div class="col-sm-5">          
                                        <input type="password" class="form-control" id="pass" placeholder="Password" name="pass" Value="#decryptedPass#">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <input type="submit" class="btn btn-primary btn-lg active" Value="Update" name="btn_Update"></a>
                                    </div>
                                </div>
                            </form>
                            
                        </cfoutput>
                    </div>
                </div> 
            </div>
            <div style="margin-top=200px!important;">
                <cfinclude template="includes/footer.cfm">
            </div>
        </body>
    </html>
</cfif>
