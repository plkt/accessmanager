<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <title>DAStek ACCESS MANAGER</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href="css/signin.css" rel="stylesheet">
            
        </head>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
        <body>
            <!-- NAVIGATION CODE START FROM HERE  -->
            <cfinclude template="includes/header.cfm">

            <div class="container-fluid">
                <div class="row">
                    <!-- SIDE NAVIGATION -->
                    <cfinclude template="includes/sideNav.cfm">
                    <div class="col-md-1 mainpage">
                        <div class="container">
                            Submitted Successfully
                        </div>
                    </div>
                </div>
            </div>
            <cfinclude template="includes/footer.cfm">
        </body>
    </html>

    <cfelse>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <title>DAStek ACCESS MANAGER</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href="css/signin.css" rel="stylesheet">
            
        </head>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
        <body>
        <!-- NAVIGATION CODE START FROM HERE  -->
            <cfinclude template="includes/header.cfm">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-1 mainpage">
                        <div class="container">
                            Submitted Successfully
                        </div>
                    </div>
                </div>
            </div>
            <cfinclude template="includes/footer.cfm">
        </body>
    </html>
</cfif>