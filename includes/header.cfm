<cfif StructKeyExists(session,"stUserLogin")>
   <nav class="navbar fixed-top navbar-light" style="background-color:rgba(40,57,101,.9);">
         <div class="container-fluid">
            <div class="navbar-header">
               <a href="homePage.cfm"><img src="css/images/DAStek_logo.png"></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
               <li><a class="a2" href="profile.cfm"><strong>Hi </strong><cfoutput>#session.stUserLogin.userName#</cfoutput></a></li>
               <li><a class="a2" href="index.cfm?logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>
         </div>
      </nav>
<cfelse>
<cflocation url ="index.cfm" addtoken="no">
</cfif>
  