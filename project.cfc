<cfcomponent >
   <cffunction name="updateProjectsData"> 
      <cfargument name="formData"  required="true" /> 
      <cfquery name="deactivateOldDetails" datasource="devendra">
         UPDATE project_master
         SET isactive = 0 
            ,updatedby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" /> 
            ,update_time = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#" /> 
         WHERE project_id = #arguments.formData.project_Id#
      </cfquery>
      <cfquery name="addNewDetailsToProject" datasource="devendra" result="varResult">
         <cfset encryptedProjectCn = encrypt(formData.project_clientname,'12345')>
         <cfset encryptedProjectCi = encrypt(formData.project_clientinfo,'12345')>
         <cfset encryptedProjectFtp = encrypt(formData.project_ftpdetails,'12345')>
         <cfset encryptedProjectGit = encrypt(formData.project_gitdetails,'12345')>
         <cfset encryptedProjectUrl = encrypt(formData.project_url,'12345')>
         <cfset encryptedProjectDb = encrypt(formData.project_dbdetails,'12345')> 
         INSERT INTO [dbo].[project_master]
         ([project_name]
         ,[project_clientname]
         ,[project_clientinfo]
         ,[project_ftpdetails]
         ,[project_gitdetails]
         ,[project_url]
         ,[project_dbdetails]
         ,[createdby]
         ,[create_time]
         ,[isactive])    
         VALUES    
         (
         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#formData.project_name#" />     
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectCn#" />     
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectCi#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectFtp#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectGit#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectUrl#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectDb#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#" />
         ,1) 
      </cfquery>
      <cfquery name="assignNewDetaildeProject" datasource="devendra">
         UPDATE user_project_mapping 
         SET projectid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#varResult.IDENTITYCOL#" />
            ,updatedby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" /> 
            ,update_time = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#" /> 
         WHERE projectid = #arguments.formData.project_Id#
      </cfquery>
     
   </cffunction>    

   <!--- Projects Name  Display on Dropdown --->
   <cffunction name="displayProjects" returntype="query">
      <cfquery name="getProject" datasource="devendra">
         SELECT  [project_id]
         ,[project_name]
         ,[project_clientname]
         ,[project_clientinfo]
         ,[project_ftpdetails]
         ,[project_gitdetails]
         ,[project_url]
         ,[createdby]
         ,[updatedby]
         ,[create_time]
         ,[update_time]
         ,[isactive]
         FROM [Training].[dbo].[project_master] WHERE isactive = 1 ORDER BY project_name ASC
      </cfquery>
      <cfreturn getProject>
   </cffunction>

   <cffunction name="specificUserProjects" returntype="query">
      <cfargument name="user_id" required="true">
      <cfquery name="getSpecificProject" datasource="devendra">
         SELECT [project_master].[project_name],
               [project_master].[project_id]
         FROM    [Training].[dbo].[project_master] 
         INNER JOIN [Training].[dbo].[user_project_mapping] 
         ON [project_master].[project_id] = [user_project_mapping].[projectid]
         WHERE [user_project_mapping].[userid]= #arguments.user_id#
      </cfquery>
      <cfreturn getSpecificProject>
   </cffunction>
   
   <cffunction name="remainingspecificUserProjects" returntype="query">
      <cfargument name="user_id" required="true">
      <cfquery name="getremainingSpecificProject" datasource="devendra">
         SELECT [project_master].[project_name],
               [project_master].[project_id]
         FROM    [Training].[dbo].[project_master] 
         INNER JOIN [Training].[dbo].[user_project_mapping] 
         ON [project_master].[project_id] = [user_project_mapping].[projectid]
         WHERE [user_project_mapping].[userid] <> #arguments.user_id#
      </cfquery>
      <cfreturn getremainingSpecificProject>
   </cffunction>
   <!----display projects which is not assigned to user
   <cffunction name="displayprojectsnotassignedtouser" returntype="query" >
      <cfquery name="qdisplayprojectsnotassignedtouser" datasource="devendra">
      SELECT 
      </cfquery>
   </cffunction>--->  

   <!--- deleting specific user's project data--->

   <!--- <cffunction name="deleteProjectsForSpecicifcUser" returntype="query">
         <cfargument name="project_id" required="true">
         <cfargument name="user_id" required="true">
         <cfquery name="qdeleteProjectsForSpecicifcUser" datasource="devendra">
            SELECT user_project_mapping 
            WHERE projectid = #arguments.project_id# AND userid= #arguments.user_id#
         </cfquery>
      <cfreturn qdeleteProjectsForSpecicifcUser> 
   </cffunction>--->
      
   <!---function for displaying normal user their projects --->
   
   <cffunction name="displayProjectsForUpdation" returntype="query">
      <cfargument name="userid" required="true">
      <cfquery name="getProjectForUpdation" datasource="devendra">
        SELECT 
			[project_master].[project_name]
			FROM    [Training].[dbo].[project_master] 
			INNER JOIN [Training].[dbo].[user_project_mapping] 
			ON [project_master].[project_id] = [user_project_mapping].[projectid]
			WHERE [user_project_mapping].[userid]=#arguments.userid# 
      </cfquery>
      <cfreturn getProjectForUpdation>
   </cffunction>

   <!--- Project List on Project Page --->

   <cffunction name="retrieveProjectsData" returntype="query">
      <cfargument name="project_id" required="true">
     
      <cfquery name="qProject" datasource="devendra">
         SELECT  [project_id]
         ,[project_name]
         ,[project_clientname]
         ,[project_clientinfo]
         ,[project_ftpdetails]
         ,[project_gitdetails]
         ,[project_url]
         ,[createdby]
         ,[updatedby]
         ,[create_time]
         ,[update_time]
         ,[project_dbdetails]
         FROM [Training].[dbo].[project_master]
         WHERE project_id = 
         <cfqueryparam cfsqltype="cf_sql_integer" value="#ARGUMENTS.project_id#">
      </cfquery>
      <cfreturn qProject>
   </cffunction>

   <!---project name in update user--->
   <cffunction name="retrieveProjects2Data" returntype="query">
      <cfargument name="project_id" required="true">
      <cfdump var="#project_id#">
      <cfquery name="qProject2" datasource="devendra">
         SELECT 
         [project_name]
         
         FROM [Training].[dbo].[project_master]
         WHERE project_id = 
         <cfqueryparam cfsqltype="cf_sql_integer" value="#ARGUMENTS.project_id#">
      </cfquery>
      <cfreturn qProject2>
   </cffunction>


   <!--- Create or Add New Project --->
   <cffunction name="createProject" returntype="query">
      <cfargument name="formData"  required="true" />
      <!---<cfdump var="#ARGUMENTS.formData#"><cfabort>--->
      <cfset encryptedProjectCn = encrypt(formData.project_clientname,'12345')>
      <cfset encryptedProjectCi = encrypt(formData.project_clientinfo,'12345')>
      <cfset encryptedProjectFtp = encrypt(formData.project_ftpdetails,'12345')>
      <cfset encryptedProjectGit = encrypt(formData.project_gitdetails,'12345')>
      <cfset encryptedProjectUrl = encrypt(formData.project_url,'12345')>
      <cfset encryptedProjectDb = encrypt(formData.project_dbdetails,'12345')>
      <cfquery name="qAddProject" datasource="devendra" result="varResult">  
         INSERT INTO [dbo].[project_master]
         ([project_name]
         ,[project_clientname]
         ,[project_clientinfo]
         ,[project_ftpdetails]
         ,[project_gitdetails]
         ,[project_url]
         ,[project_dbdetails]
         ,[createdby]
         ,[create_time]
         ,[isactive])    
         VALUES    
         (
         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#formData.project_name#" />     
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectCn#" />     
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectCi#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectFtp#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectGit#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectUrl#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedProjectDb#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" />
         ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#dateTimeFormat(now())#" />
         ,1)     
      </cfquery>  
      <cflocation url = "response.cfm"> 
   </cffunction>

   <!--- User Screen Display --->

   <!---<cffunction name="userProjectInfo" returntype="query">
       <cfargument name="myproject" required="true"> 
     
         <cfquery name="qProjectsinfo" datasource="devendra"> 
             SELECT  [project_id]   
          			,[project_name]   
 					   ,[project_clientname]       
                 ,[project_clientinfo]       
                 ,[project_ftpdetails]       
                 ,[project_gitdetails]       
                 ,[project_url]       
                 ,[createdby]       
                 ,[updatedby]       
                 ,[create_time]       
                 ,[updated_time]       
                 ,[project_dbdetails]       
                 FROM [Training].[dbo].[project_master]    
    			 WHERE project_id =     
             <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.myproject#"> AND isactive=1 ;     
                    
            </cfquery>       
                
    		<cfreturn qProjectsinfo>
          
   </cffunction>---->


   <!---Update the project status--->

   <cffunction name="projectsDelete">
      <cfargument name="project_id" required="true">
      <cfquery name="qDeleteProjects" datasource="devendra">
         UPDATE [dbo].[user_project_mapping]
         SET isactive  = 0
         WHERE projectid  = #arguments.project_id# 
         
         UPDATE [dbo].[project_master]
         SET isactive  = 0
         WHERE project_id  = #arguments.project_id# 
      </cfquery>
   </cffunction>

</cfcomponent>

      




















