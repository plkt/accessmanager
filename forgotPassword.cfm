<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Devendra Shukla">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>DAStek Access Manager</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Custom styles for this template -->
        <link href="css/signin.css" rel="stylesheet"> 
        <link href="css/main.css" rel="stylesheet"> 
    </head>
   
    <body class="text-center">
        <div class="login-html">
            <!---Form Processiong begin here---->
            <cfif structkeyExists(form,'send_Link')>
                <!---create an instance of the authentication service --->
                <cfset checkobj = createobject("component",'AccessManager.authenticationService')/>   
                <!-- Server side data validation -->
                <cfset Checked =  checkobj.check_Email(form.fld_userEmail2) />
            </cfif>
            <!---Form Processing end here--->
            
            <form class="form-signin" id="frmConnexion" preservedata="true"  method="post">
                <h1 class="loginH1">DAStek Access Manager</h1>
                <img class="mb-4 profileImage" src="css/images/img_avatar.png" alt="" width="100" height="108" >
                <br><br/>
                <label for="fld_userEmail" class="sr-only">Email address</label>
                <input type="email" name="fld_userEmail2" id="fld_userEmail" required="true"  validate="email" validateAt="onSubmit"class="form-control" placeholder="Email address" ><br/>
                <input type="submit" name="send_Link" id="fld_submitLogin"class="btn btn-primary btn-lg active btnn" value="Verify" >
                <br><br>
                <span><a class="a1" href="index.cfm">Sign in</a></span>
                
                <p class="loginP">
                    <cfset yearNow = #Year(now())# />
                    <cfoutput>
                        &copy; Copyright #yearNow# <a class="a1" href="http://www.dasteksoftwares.com/" target="_blank">DAStek Softwares</a>
                    </cfoutput>
                </p>
            </form>
        </div>
    </body>
</html>