<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
         <link href="css/main.css" rel="stylesheet">
      </head>
      <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body >
         <!--- <cfset projectObj = createobject("component",'project')/> --->
         <!--- 	  <cfset getallProjects=projectObj.displayProjects()/> --->

         <cfif StructKeyExists(URL,'userId')>
            <cfset VARIABLES.userid = URL.userId>
         <cfelseif  StructKeyExists(FORM,'userid')>
            <cfset VARIABLES.userid = FORM.userid>
         </cfif>
         
         <cfset projectObj = createobject("component",'project')/>
         <cfset  qProject = projectObj.specificUserProjects(FORM.userid)/>
         <cfset listProjects = valueList(qProject.project_name)>

         <cfset userObj = createobject("component",'user')/>
         <cfset getallUsersData=userObj.retrieveUsersData(userId = VARIABLES.userid)/>
         
         
         <cfset getallProjects=projectObj.displayProjects()/>
         
         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template = "includes/header.cfm">
         <!-- NAVIGATION END -->
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <cfinclude template="includes/sideNav.cfm">
               <!-- SIDE NAVIGATION END-->
               <!-- MAINPAGE CODE -->

               <cfif StructKeyExists(FORM,'btn_userUpdate')>
                 
                  <cfset qUpdateUser=userObj.setUsersData(FORM)/>
                  <META HTTP-EQUIV="REFRESH" CONTENT="0">
                  
               </cfif>

               <div class="col-md-1 mainpage" ></div>
               <div class="col-md-6 mainpage" >
                  <h2><b>UPDATE USER</b></h2><hr>
                  <cfoutput query="getallUsersData">
                        
                     <form class="form-horizontal" method="POST">
                        <input type="hidden" name="userId" value="#userid#">
                        
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="uname">USER'S NAME:</label>
                           <div class="col-sm-5">
                              <input type="text" class="form-control" id="user_name" placeholder=" User's Name" name="uname" value="#user_name#">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="user_email">EMAIL:</label>
                           <div class="col-sm-5">
                              <input type="email" class="form-control" id="user_email" placeholder=" email" name="email" value="#user_email#">
                           </div>
                        </div>

                        <div class="form-group">
                           <label class="control-label col-sm-3" for="user_password">PASSWORD:</label>
                           <div class="col-sm-5">   
                        
                              <cfset decryptedPass = decrypt(user_password,'12345')>
                              <input type="password" class="form-control" id="user_password" placeholder="XXXXXXX" name="user_pass" value="#decryptedPass#">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="user_role">ROLE:</label>
                           <div class="col-sm-5">   
                        
                              <cfif getallUsersData.user_roleid eq 1>
                                 <input type="text" class="form-control" id="user_role" placeholder="userrole" name="user_role" value="admin" readonly>
                              <cfelse>
                                 <input type="text" class="form-control" id="user_role" placeholder="userrole" name="user_role" value="user" readonly>
                              </cfif>
                              
                           </div>
                        </div>

                        <div class="form-group">
                           <label class="control-label col-sm-3" for="project_name">PROJECT:</label>
                           <div class="col-sm-5">
                     

                             
                           <select class="form-control"  name="project_id" id="fld_userProject" value="project_id"  display="project_name" multiple>
                              <cfloop query="getallProjects">
                                 <option value="#project_id#" <cfif listFindNoCase(listProjects,getallProjects.project_name)>selected</cfif>>#getallProjects.project_name#</option>   
                              </cfloop>
                           </select>

                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-3 col-sm-10">
                              <input type="submit" name="btn_userUpdate" id="btn_userUpdate"class="btn btn-primary btn-lg active" value="Update User" >
                           </div>
                        </div>
                     </form>
                  </cfoutput>
               </div>
               <!-- MAINPAGE CODE END -->	
            </div>

         </div>
         <cfinclude template="includes/footer.cfm">
      </body>
   </html>
   <cfelse>
   You ae not allowed to access this page.
</cfif>