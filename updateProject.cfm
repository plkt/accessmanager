<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <cftry>
      <!DOCTYPE html>
      <html lang="en">
         <head>
            <title>DAStek ACCESS MANAGER</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href="css/signin.css" rel="stylesheet">
            <link href="css/main.css" rel="stylesheet">
         </head>
         <script>
            $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
            });
         </script>
         <body >
            <cfif StructKeyExists(URL,'project_Id')>
            <cfset VARIABLES.project_id = URL.project_Id>
            <cfelseif  StructKeyExists(FORM,'project_Id')>
            <cfset VARIABLES.project_id = FORM.project_Id>
            </cfif>
            <cfset projectObj = createobject("component",'project')/>
            <cfset getallProjectData=projectObj.retrieveProjectsData(project_Id = VARIABLES.project_id)/>
   
            <!--- cfdump var= "#getallProjectData#"> <cfabort>  --->
            <!---- <cfdump var="#getallProjects#"/><cfabort> --->
            <!-- NAVIGATION CODE START FROM HERE  -->
            <cfinclude template = "includes/header.cfm">
            <!-- NAVIGATION END -->
            <div class="container-fluid">
               <div class="row">
                  <!-- SIDE NAVIGATION -->
                  <cfinclude template="includes/sideNav.cfm">
                  <!--- <cfdump var = "#form#"> --->
            
                  <cfif StructKeyExists(FORM,'fld_submitUpdate') AND FORM.fld_submitUpdate EQ 'Update'>
               
                  <cfset qUpdateProject=projectObj.updateProjectsData(FORM)/>
                  <META HTTP-EQUIV="REFRESH" CONTENT="0">
                  </cfif>

                  <!-- SIDE NAVIGATION END-->
                  <!-- MAINPAGE CODE -->
                  <div class="col-md-1 mainpage" ></div>
                  <div class="col-md-6 mainpage" >
                     <h2><b>Update Project Details</b></h2><hr>
               
                     <cfoutput>
                        <form class="form-horizontal" method="post">
                           <input type="hidden" name="action" value="submit-form">
                           <input type="hidden" name="project_Id" value="#getallProjectData.project_Id#">
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_name">PROJECT NAME:</label>
                              <div class="col-sm-5">
                                 <input type="text" class="form-control" id="clntname" placeholder=" CLIENT's Name" name="project_name" value="#getallProjectData.project_name#" readonly> 
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_clientname">CLIENT's Name:</label> 
                              <div class="col-sm-5"> 
                              <cfset decryptedProjectCn = decrypt(getallProjectData.project_clientname,'12345')>
                                 <input type="text" class="form-control" id="project_clientname" placeholder=" CLIENT's Name" name="project_clientname" value="#decryptedProjectCn#"> 
                              </div>
                           </div>
                           <div class="form-group">    
                              <label class="control-label col-sm-3" for="project_clientinfo">CLIENT INFO:</label>      
                              <div class="col-sm-5">  
                                 <cfset decryptedProjectCi = decrypt(getallProjectData.project_clientinfo,'12345')>  
                                 <textarea name="project_clientinfo" rows="3" cols="70" value="#decryptedProjectCi#">#decryptedProjectCi#</textarea>       
                              </div>    
                           </div> 
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_ftpdetails">FTP DETAILS:</label> 
                              <div class="col-sm-5">
                                 <cfset decryptedProjectFtp = decrypt(getallProjectData.project_ftpdetails,'12345')>    
                                 <textarea name="project_ftpdetails" rows="3" cols="70" value="#decryptedProjectFtp#">#decryptedProjectFtp#</textarea>    
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_gitdetails">GIT DETAILS:</label>     
                              <div class="col-sm-5">    
                                 <cfset decryptedProjectGit = decrypt(getallProjectData.project_gitdetails,'12345')>     
                                 <textarea name="project_gitdetails" rows="3" cols="70" value="#decryptedProjectGit#">#decryptedProjectGit#</textarea>    
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_dbdetails">DB DETAILS:</label>    
                              <div class="col-sm-5">  
                                 <cfset decryptedProjectDb = decrypt(getallProjectData.project_dbdetails,'12345')>    
                                 <textarea name="project_dbdetails" rows="3" cols="70" value="#decryptedProjectDb#">#decryptedProjectDb#</textarea>    
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="project_url">URL:</label>     
                              <div class="col-sm-5">   
                                 <cfset decryptedProjectUrl = decrypt(getallProjectData.project_url,'12345')>  
                                 <textarea name="project_url" rows="3" cols="70" value="#decryptedProjectUrl#">#decryptedProjectUrl#</textarea>    
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-10"> 
                                 <input type="submit" name="fld_submitUpdate" id="fld_submitUpdate"class="btn btn-primary btn-lg active" value="Update" >
                              </div>
                           </div>
                        </form>
                     </cfoutput>
                  </div>
                  <!-- MAINPAGE CODE END  -->
               </div>
            </div>
         </body>
      </html>
      <cfcatch type="any">
         <cfdump var = "#cfcatch#">
      </cfcatch>
   </cftry>
   <cfelse>
   You are not allowed to access this page.
</cfif>