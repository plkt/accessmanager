<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
         <link href="css/myCss.css" rel="stylesheet">
      </head>
      <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body>
         <cfset userObj = createobject("component",'user')/>
         <cfset getallUsers=userObj.retrieveUsers()/>

         <cfset projectObj = createobject("component",'project')/>
         <cfset getallProjects=projectObj.displayProjects()/>
      
         <cfif StructKeyExists(FORM,'delete_submit') AND FORM.delete_submit EQ 'Delete'>
            <cfset deleteUsers=userObj.usersDelete(FORM.userid)/> 
            <META HTTP-EQUIV="REFRESH" CONTENT="0">
         </cfif>

         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template="includes/header.cfm">
         <!-- NAVIGATION END -->
         
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
              <cfinclude template="includes/sideNav.cfm">
               <!-- SIDE NAVIGATION END-->
               <!-- MAINPAGE CODE -->
               <div class="col-md-1 mainpage" ></div>
               <div class="col-md-6 mainpage" >
                  <h2><b>USER'S LIST</b></h2>
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>USER NAME</th>
                           <th>ACTION</th>
                        </tr>
                     </thead>
                     <tbody>
                        <cfoutput query="getallUsers">
                           <tr>
                              <form method="POST" action="userUpdate.cfm">
                                 <input type="hidden"  name="userid" value="#getallUsers.userid#">
                                 <td><input class="btn btn-link" type="submit" value="#user_name#"/></td>
                              </form>
                              <form method="post">
                                 <input type="hidden"  name="userid" value="#getallUsers.userid#">
                                 
                                 <td><input class="btn btn-link" type="submit" value="Delete" name="delete_submit" /></td>
                              </form>
                           </tr>
                        </cfoutput>
                     </tbody>
                  </table>
               </div>
               <!-- MAINPAGE CODE END -->	
            </div>
         </div>
         <!---footer--->
         <cfinclude template="includes/footer.cfm">
      </body>
   </html>
   <cfelse>
   You are not allowed to access this page.
</cfif>
