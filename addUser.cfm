<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
         
      </head>
      <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body>
         <cfset projectObj = createobject("component",'project')/>
         <cfset getallProjects=projectObj.displayProjects()/>
         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template="includes/header.cfm">
         <!-- NAVIGATION END -->
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <cfinclude template="includes/sideNav.cfm">
            
               <cfif structkeyExists(form,'fld_AddUser')>
                  <cfset userObj = createobject("component",'user')/>
                  <cfset qcreateUsers=userObj.createNewUser(form)/>
               </cfif>
               <!-- SIDE NAVIGATION END-->
               <!-- MAINPAGE CODE -->
               <div class="col-md-1 mainpage"></div>
               <div class="col-md-6 mainpage">
                  <h2><b>ADD USER</b></h2>
                  <hr>
                  <form class="form-horizontal" method="post">
                     <input type="hidden" name="action" value="submit-form">
                     <input type="hidden" name="userId" value="#userId#">
                     <div class="form-group left-align">
                        <label class="control-label col-sm-3" for="user_name" style="text-align:left!important; ">USER'S NAME:</label>
                        <div class="col-sm-5">
                           <input type="text" class="form-control" id="user_name" placeholder=" User's Name" name="user_name">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="user_email">EMAIL:</label>
                        <div class="col-sm-5">
                           <input type="email" class="form-control" id="user_email" placeholder=" email" name="user_email">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="user_password">PASSWORD:</label>
                        <div class="col-sm-5">          
                           <input type="password"  class="form-control" id="user_password" placeholder="password" name="user_password">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="uname">PROJECT NAME:</label>
                        <div class="col-sm-5">
                           <select class="form-control"  name="project_id" id="fld_userProject" value="project_id" display="project_name" multiple>
                                 <option value="0">SELECT PROJECT</option>
                                 <cfoutput query="getallProjects">
                                    <option value="#project_id#">#project_name#</option>
                                 </cfoutput>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="contactno">CONTACT NO.:</label>
                        <div class="col-sm-5">          
                           <input type="text"  class="form-control" id="contactno" placeholder="Contact No." name="contactno">
                        </div>
                     </div>
                     
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="user_role">ROLE:</label>   
                        <div class="col-sm-5">          
                           <input type="radio" name="user_role" id="admin" value="1">
                           <label for="admin">Admin</label>  
                           <input type="radio" name="user_role" value="2" id="user" checked>
                           <label for="user">User</label> 
                        </div>
                     </div>
                     
                     <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                           <input type="submit" name="fld_AddUser" id="fld_AddUser"class="btn btn-primary btn-lg active btnn" value="Submit" >
                        </div>
                     </div>
                  </form>
               </div>
               <!--- MAINPAGE CODE END --->	
            </div>
         </div>
         <div style="margin-top:136px!important;">
            <cfinclude template="includes/footer.cfm">
         </div>
      </body>
   </html>

   <cfelse>
   You are not allowed to access this page.
</cfif>