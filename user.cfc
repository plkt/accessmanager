
<!--- Showing Users in home page --->
<cfcomponent >
   <cffunction name="retrieveUsers" returntype="query">
      <cfquery name="getUsers" datasource="devendra">
         SELECT [userid]
         ,[user_name]
         ,[user_email]
         ,[user_password]
         ,[user_roleid]
         ,[user_isactive]
         ,[createdby]
         ,[updatedby]
         ,[create_time]
         ,[updated_time]
         FROM [Training].[dbo].[user_master]  WHERE [user_roleid]=2 AND user_isactive=1 ORDER BY user_name;
      </cfquery>
      <cfreturn getUsers>
   </cffunction>
   
   <cffunction name="retrieveUsersData" returntype="query">
      <cfargument name="userId" required="true">
     
      <cfquery name="qUser" datasource="devendra">
         SELECT [userid]
         ,[user_name]
         ,[user_email]
         ,[user_password]
         ,[user_roleid]
         ,[user_isactive]
         ,[createdby]
         ,[updatedby]
         ,[create_time]
         ,[updated_time]
         ,[contact_no]
         FROM [Training].[dbo].[user_master]
         WHERE userid = 
         <cfqueryparam cfsqltype="cf_sql_integer" value="#ARGUMENTS.userid#">
      </cfquery>
      <cfreturn qUser>
   </cffunction>

  <!--- doing work for userUpdate --->
   <cffunction name="retrvProjectsData" returntype="query">
      <cfargument name="userId" required="true">

      <cfquery name="qretrvProjectsData" datasource="devendra" >
         SELECT 
          [project_master].[project_name]
          FROM    [Training].[dbo].[project_master] 
          INNER JOIN [Training].[dbo].[user_project_mapping] 
          ON [project_master].[project_id] = [user_project_mapping].[projectid]
          WHERE [user_project_mapping].[userid]=<cfqueryparam cfsqltype="cf_sql_integer" value="#ARGUMENTS.userid#">
     </cfquery>
     
            <cfloop query="qretrvProjectsData">
               <cfset listProjects = "">
               <cfset finallistProjects = listAppend (listProjects, "#qretrvProjectsData.project_name#" ) >
            </cfloop>
   
      <cfreturn finallistProjects>
   </cffunction>

   <!---Update Profile--->
   <cffunction name="updateProfile" returntype="query">
      <cfargument name="formData" required="true">
      <cfset encryptedPass = encrypt(formData.pass,'12345')>
      <cfquery name="qupdateUserProfile" datasource="devendra">
         UPDATE [Training].[dbo].[user_master] 
         SET 
         user_name =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.uname#" />
         ,user_email =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.email#" />
         ,contact_no = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.contactno#" />
         ,user_password = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedPass#" />
         ,updatedby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" />
         ,updated_time = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#" />
         WHERE 
         userId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#session.stUserLogin.userId#">

      </cfquery>
      <cflocation url = "response.cfm" addtoken="no">
      <cfreturn qupdateUserProfile>
    
   </cffunction>


   <!-------------------------------------------------- userUpdate --------------------------------------->
   <cffunction name="setUsersData">
      <cfargument name="formData" required="true">
      
      <cfset encryptedPass = encrypt(formData.user_pass,'12345')>
      <cfquery name="qupdateUser" datasource="devendra">
         UPDATE [Training].[dbo].[user_master] 
         SET user_name =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.uname#" />
         ,user_email =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.email#" />
         ,user_password = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedPass#" />
         ,updatedby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" />
         ,updated_time = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#" />
         WHERE 
         userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.formData.userId#">

      </cfquery>
      
      <cfloop list="#ARGUMENTS.formData.project_id#" index="listItem">
     
			
         <cfquery name="qInsertNewProjectForUSer" datasource="devendra">
            
            
            INSERT INTO user_project_mapping
            (userid,projectid,isactive,updatedby,update_time) 
            VALUES
            (
            <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#formData.USERID#" />
            ,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listItem#"/> 
            ,1
            ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#"/>  
            ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#"/> 
            );
            
         </cfquery>	
		</cfloop>
    
      
     
 
     
      <cflocation url="response.cfm">
      
      <!--- <cfdump var="#ARGUMENTS.formData#"><cfabort> --->
      
   </cffunction>
   <!-------------------------------- add new user ------------------------------->
   <cffunction name="createNewUser" returntype="query">
      <cfargument name="formData"  required="true" />
     
      <!---encryption of password--->
      <cfset encryptedPass = encrypt(formData.user_password,'12345')>
      <cftry>
         <cfquery name="quserCretaion" datasource="devendra" result="varResult">  
            INSERT INTO [dbo].[user_master]    
                  (
                     [user_name]     
                     ,[user_email]     
                     ,[user_password]  
                     ,[user_roleid]  
                     ,[user_isactive]
                     ,[createdby]
                     ,[contact_no]
               )     
                        
            VALUES    
                  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.user_name#" />     
                  ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.user_email#" />     
                  ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#encryptedPass#" />
                  ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.user_role#" />
                  ,1
                  ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#" />  
                  ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.formData.contactno#"  />  
                  )     
               
         </cfquery>  
         <!---<cfset myArray = [#ARGUMENTS.formData.project_id#] />	--->
			<!--- <cfdump var="#myArray#">      <cfabort>   --->
			
			<cfloop list="#ARGUMENTS.formData.project_id#" index="listItem">
			
				<cfquery name="qupdateUser" datasource="devendra">
					
					
				   INSERT INTO user_project_mapping(userid,projectid,isactive,createdby,create_time) VALUES(
				   <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#varResult.IDENTITYCOL#" />
				   ,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listItem#"/> 
				   ,1
               ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#session.stUserLogin.userName#"/>  
               ,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateTimeFormat(now())#"/> 
               );
				 
				</cfquery>	
			</cfloop>
			
		
		<cfcatch type="any">
      <cfdump var = "#cfcatch#"><cfabort>
      </cfcatch>
      </cftry>
      <cflocation url = "response.cfm">
   </cffunction>




   <!---Update the user status--->

   <cffunction name="UsersDelete">
      <cfargument name="userid" required="true">
      
      <cfquery name="qDeleteUsers" datasource="devendra">



         UPDATE [dbo].[user_master]
         SET user_isactive  = 0
         WHERE userid  = #arguments.userid# 

      </cfquery>
   
   </cffunction>



   <cffunction name="userProfile" returntype="query">
      <cfargument name="userid" required="true">
      
      <cfquery name="qProfileUser" datasource="devendra">

         SELECT user_name FROM user_master
         WHERE user_email  = '#arguments.userid#'

      </cfquery>
      <cfreturn qProfileUser> 
   </cffunction>



   <cffunction name="Profile" returntype="query">
      <cfargument name="userid" required="true">
      
      <cfquery name="qProfile" datasource="devendra">

         SELECT * FROM user_master
         WHERE user_id  = '#arguments.userid#'

      </cfquery>
      <cfreturn qProfile> 
   </cffunction>

</cfcomponent>