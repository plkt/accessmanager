<link href="css/signin.css" rel="stylesheet">
<div class="col-md-3 sidenav"  >
    <div class="nav-container">
        <ul class="nav">
        <li class="active">
            <a href="usersList.cfm">
            <span class="textclass" data-toggle="tooltip" data-placement="right" title= "MANAGE USERS">USERS</span>
            </a>
        </li>
        <li class="active">
            <a href="addUser.cfm">
            <span class="textclass" data-toggle="tooltip" data-placement="right" title= "CREATE NEW USER">ADD NEW USER</span>
            </a>
        </li>
        <li>
            <a href="projectList.cfm">
            <span class="textclass" data-toggle="tooltip" data-placement="right" title="MANAGE PROJECT">PROJECT</span>
            </a>
        </li>
        <li>
            <a href="addproject.cfm">
            <span class="textclass" data-toggle="tooltip" data-placement="right" title="ADD PROJECT">ADD NEW PROJECT</span>
            </a>
        </li>
        </ul>
    </div>
</div>