<cfcomponent output="false" name="AccessManager">
	<!--- -Validate User Method --->
	<cffunction name="validateUser" access="public" output="false" returntype="array">
		<cfargument name="userEmail" type="string" required="true" />
		<cfargument name="userPassword" type="string" required="true" />
		<!---<cfdump var= "#arguments#"><cfabort>--->
		<cfset aErrorMessages = ArrayNew(1) />
		<!---Validate the email---->
		<cfif NOT isValid('email',arguments.userEmail)>
			<cfset arrayappend(aErrorMessages,'Please ,provide a valid eMail address') />
		</cfif>
		<!---<cfdump var="#aErrorMessages#"><cfabort> ok --->
	
		<!---Validate the password---->
		<cfif arguments.userPassword EQ ''>
			<cfset arrayappend(aErrorMessages,'Please ,provide a password') />
		</cfif>
		<cfreturn aErrorMessages />
	</cffunction>
	
	<!---- do Login Method ---->
	<cffunction name="doLogin" access="public" output="false" returntype="boolean">
		<cfargument name="userEmail" type="string" required="true" />
		<cfargument name="userPassword" type="string" required="true" />
		<cfset encryptedPass = encrypt(arguments.userPassword,'12345')>
		<!---<cfdump var= "#arguments#"><cfabort> ok --->
		<!--- Create the isUserLoggedIn variable --->
		<cfset var isUserLoggedIn= false />
		<!--- Get the user data from database --->
		<cfquery name="rsLoginUser" datasource="devendra">
			SELECT 
					[user_master].[userid], 
					[user_master].[user_name],
					[user_master].[user_email],
					[user_master].[user_password], 
					[user_master].[user_roleid], 
					[user_master].[user_isactive], 
					[userrolemaster].[user_role_desc] 
			FROM    [Training].[dbo].[user_master] 
			INNER JOIN [Training].[dbo].[userrolemaster] 
				ON [user_master].[user_roleid] = [userrolemaster].[user_roleid]
			WHERE  [user_master].[user_email]='#arguments.userEmail#' and 
					[user_master].[user_password]= '#encryptedPass#' and 
					[user_master].[user_isactive] = 1;
		</cfquery>
		<!--- <Cfdump var="#rsLoginUser#"><cfabort> ok --->
		<!----Check if the qury returns one and only one user---->
		<cfif rsLoginUser.recordCount eq 1>
			<!---save the user data in session--->
			<cfset SESSION.stUserLogin = {'userId'=rsLoginUser.userid, 'userName'=rsLoginUser.user_name, 'isAdmin'=IIF(rsLoginUser.user_role_desc EQ 'Admin',DE('1'),DE('0'))}>
			<cfset var isUserLoggedIn=true />
		</cfif>
		<cfreturn isUserLoggedIn />
	</cffunction>
	
	<cffunction name="doLogout" access="public" output="false" returntype="void">
		<cfset StructDelete(session,'stUserLogin')>
	</cffunction>
	
	<!--- checks email for forgot password --->
	<cffunction name="check_Email">
		<cfargument name="Email" type="string" required="true" />
		<!---<cfdump var= "#arguments.Email#"><cfabort>--->
		<cfquery name="myQuery" datasource="devendra">
			SELECT [user_password]
			FROM [dbo].[user_master] WHERE user_email = '#arguments.Email#'
		</cfquery>
		<!--- <cfdump var="#myQuery#"><cfabort>---->
		<cfset cond="0">
		<cfif myQuery.recordCount>
			<cfset cond="1">
		</cfif>
		<cfif cond eq "1">
		<cfset decryptedPass = decrypt(myQuery.user_password,'12345')>
			<cfmail from="pulkit.bisht@dasteksoftwares.com" to="pulkit.dastek@gmail.com" subject="Regarding verification">
				your password is: #decryptedPass#
			</cfmail>
		</cfif>
	</cffunction>

	<!---<cffunction name="check_Email" access="public" output="false" returntype="boolean">
		<cfargument name="email" required="true">
		<cfdump var="#arguments.email#"><cfabort>
   		<cfquery name="qverifyEmail" datasource="devendra">
			SELECT [dbo].[user_master]
			WHERE user_email  = #arguments.email# 
		</cfquery>

	<cfset cond=0>
		<cfif qverifyEmail.recordCount>
			<cfset cond=1>
				<cfif cond eq 1>
					Verification sent and your password is: #qverifyEmail.user_password#
					<!---<cfmail from="pulkitbisht8586@gmail.com" to="#arguments.email#" subject="Verification link">
						#qverifyEmail.user_password#
					</cfmail>--->
					<cfelse>
					Not satisfied with your email 
				</cfif>
	</cffunction>---->
	
</cfcomponent>