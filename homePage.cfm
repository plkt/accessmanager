
<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!--- admin side code--->
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
         <link href="css/main.css" rel="stylesheet">
      </head>
      <script>
         $(document).ready(function(){
           $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body>
     
         <cfset userObj = createobject("component",'user')/>
         <cfset getallUsers=userObj.retrieveUsers()/>
     
         <cfif StructKeyExists(FORM,'delete_submit') AND FORM.delete_submit EQ 'Delete'>
            <cfset deleteUsers=userObj.usersDelete(FORM.userid)/> 
            <META HTTP-EQUIV="REFRESH" CONTENT="0">
            <cfset Profileuser = userObj.userProfile(form.username)>
         </cfif>
   
         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template="includes/header.cfm">
         <!-- NAVIGATION END -->
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <cfinclude template="includes/sideNav.cfm">
               <!-- SIDE NAVIGATION END-->
               
               <!-- MAINPAGE CODE -->
               <div class="col-md-1 mainpage" ></div>
               <div class="col-md-6 mainpage">
                  <h2 ><b>USER'S LIST</b></h2>
                  <table class="table table-hover" >
                     <thead>
                        <tr>
                           <th>USER NAME</th>
                           <th>ACTION</th>
                        </tr>
                     </thead>
                     <tbody>
                        <cfoutput query="getallUsers">
                           <tr>
                              <form method="POST" action="userUpdate.cfm">
                                 <input type="hidden"  name="userid" value="#getallUsers.userid#">
                                 <td><input class="btn btn-link" type="submit" value="#user_name#"/></td>
                              </form>
                              
                              <form method="post">
                                 <input type="hidden" name="userid" value="#getallUsers.userid#">
                                 <td><input class="btn btn-link" type="submit" value="Delete" name="delete_submit" /></td>
                              </form>
                           </tr>
                        </cfoutput>
                     </tbody>
                  </table>
               </div>
               <!-- MAINPAGE CODE END -->	
            </div>
         </div>
         <cfinclude template="includes/footer.cfm">
      </body>
   </html>

<cfelse>
      <!----OLD CODE  which is working---->
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
      </head>
      <body >
         <cfset projectObj = createobject("component",'project')/>
         <cfset getallProjects = projectObj.specificUserProjects(session.stUserLogin.userId)/>
         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template="includes/header.cfm">
         <!-- NAVIGATION END -->
         <cfif StructKeyExists(FORM,'action') AND FORM.action EQ 'Submit'>
         <cfset qpoppulateProj=projectObj.retrieveProjectsData(FORM.myproject)/>
         </cfif>
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-3 mainpage" ></div>
               <div class="col-md-6 mainpage" >
                  <h2>SELECT YOUR PROJECT</h2>
                  <!-- MAINPAGE CODE -->
                  <!--- <cfdump var ="#form#"> --->
                  <form  method="post" action="homePage.cfm">
                     <input type="hidden" name="action" value="Submit">
                        <h2 style="text-align:center;"><b></b></h2>
                        <cfoutput>
                           <div class="form-group">
                              <label class="control-label col-sm-3" for="uname">SELECT PROJECT:</label>
                              <div class="col-sm-5">
                                 <select class="form-control"  name="myproject" onchange="this.form.submit()">
                                    <option value="0">SELECT PROJECT</option>
                                    <cfloop query="getallProjects">
                                       <option value="#project_id#">#project_name#</option>
                                    </cfloop>
                                 </select>
                              </div>
                           </div>
                        </cfoutput>
                  </form>
               </div>
            </div>  <br>    
                  <!--- <cfoutput query="qPopProj"> --->
            <cfif structKeyExists(FORM,'action') >
            <div class="row">
               <div class="col-md-3 mainpage" ></div>
               <div class="col-md-6 mainpage">
                  <cfoutput query="qpoppulateProj">
                     <form class="form-horizontal">
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="project_name">PROJECT NAME:</label>
                           <div class="col-sm-5">
                              <input type="text" class="form-control" id="project_name" placeholder="projectname" name="project_name" value="#qpoppulateProj.project_name#" readonly> 
                           </div>
                        </div>   
                        <div class="form-group">
                              <cfset decryptedProjectFtp = Decrypt(qpoppulateProj.project_ftpdetails,'12345')>
                              <label class="control-label col-sm-3" for="project_ftpdeatails">FTP DETAILS:</label>
                              <div class="col-sm-5">
                                 <textarea name="project_ftpdetails" rows="3" cols="35" value="#decryptedProjectFtp#" readonly>#decryptedProjectFtp#</textarea>
                              </div>
                        </div> 
                        <div class="form-group">
                              <cfset decryptedProjectGit = Decrypt(qpoppulateProj.project_gitdetails,'12345')>
                              <label class="control-label col-sm-3" for="project_gitdeatails">GIT DETAILS:</label>
                              <div class="col-sm-5">
                                 <textarea name="project_ftpdetails" rows="3" cols="35" value="#decryptedProjectGit#" readonly>#decryptedProjectGit#</textarea>
                              </div>
                        </div> 
                        <div class="form-group">
                              <cfset decryptedProjectDb = Decrypt(qpoppulateProj.project_dbdetails,'12345')>
                              <label class="control-label col-sm-3" for="dbdeatails">DB DETAILS:</label>
                              <div class="col-sm-5">
                                 <textarea name="project_ftpdetails" rows="3" cols="35" value="#decryptedProjectDb#" readonly>#decryptedProjectDb#</textarea>
                              </div>
                        </div> 
                        <div class="form-group">
                              <cfset decryptedProjectUrl = Decrypt(qpoppulateProj.project_url,'12345')>
                              <label class="control-label col-sm-3" for="urldeatails">URL DETAILS:</label>
                              <div class="col-sm-5">
                                 <textarea name="project_ftpdetails" rows="3" cols="35" value="#decryptedProjectUrl#" readonly>#decryptedProjectUrl#</textarea>
                              </div>
                        </div>
                     </form>
                  </cfoutput>
               </div>           
            </div>
         </cfif>
         </div>
      <cfinclude template="includes/footer.cfm">
      </body>
   </html>
</cfif>