<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!DOCTYPE html>
   <html lang="en">
      <head>
      <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
      </head>
      <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body >
         <!-- NAVIGATION CODE START FROM HERE  -->
         <nav class="navbar navbar-light" style="background-color: #563d7c;">
            <div class="container-fluid">
               <div class="navbar-header">
               <h4><b><a class="" href="homePage.cfm">DAStek ACCESS MANAGER</a></b></h4>
               </div>
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="#">Hi Devendra</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
               </ul>
            </div>
         </nav>
         <!-- NAVIGATION END -->
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <div class="col-md-3 sidenav"  >
                  <div class="nav-container">
                     <ul class="nav">
                        <li class="active">
                           <a href="addUser.cfm">
                           <span class="textclass" data-toggle="tooltip" data-placement="right" title= "CREATE NEW USER">ADD NEW USER</span>
                           </a>
                        </li>
                        <li>
                           <a href="projectList.cfm">
                           <span class="textclass" data-toggle="tooltip" data-placement="right" title="MANAGE PROJECT">PROJECT</span>
                           </a>
                        </li>
                  <li>
                        <a href="addproject.cfm">
                           <span class="textclass" data-toggle="tooltip" data-placement="right" title="ADD PROJECT">ADD NEW PROJECT</span>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
               <!-- SIDE NAVIGATION END-->
               <!-- MAINPAGE CODE -->
               <div class="col-md-6 mainpage" >
                  <h2 style="text-align:center;"><b>Add USER</b></h2>
                  <form class="form-horizontal" action="/action_page.php">
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="uname">USER'S NAME:</label>
                        <div class="col-sm-5">
                           <input type="text" class="form-control" id="uname" placeholder=" User's Name" name="uname">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="email">EMAIL:</label>
                        <div class="col-sm-5">
                           <input type="email" class="form-control" id="email" placeholder=" email" name="email">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">PASSWORD:</label>
                        <div class="col-sm-5">          
                           <input type="password" class="form-control" id="pwd" placeholder=" password" name="pwd">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="uname">PROJECT NAME:</label>
                        <div class="col-sm-5">
                           <input type="text" class="form-control" id="uname" placeholder=" Project Name" name="uname">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                           <a href="#" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Submit</a>
                        </div>
                     </div>
                  </form>
               </div>
               <!-- MAINPAGE CODE END -->	
            </div>
         </div>
         <cfinclude template = "footer.cfm">
      </body>
   </html>
   <cfelse>
   You are not allowed to access this page.
</cfif>