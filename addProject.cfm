<cfif  StructKeyExists(SESSION,'stUserLogin') and SESSION.stUserLogin.isAdmin>
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <title>DAStek ACCESS MANAGER</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="css/signin.css" rel="stylesheet">
      </head>
      <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
         });
      </script>
      <body >
         <!-- NAVIGATION CODE START FROM HERE  -->
         <cfinclude template="includes/header.cfm">
         <!-- NAVIGATION END -->
         
         <div class="container-fluid">
            <div class="row">
               <!-- SIDE NAVIGATION -->
               <cfinclude template="includes/sideNav.cfm">
               
               <cfif structkeyExists(FORM,'btn_AddProject')>
                  <!---function for adding project in database--->
                  <cfset projectObj = createobject("component",'project')/>
                  <cfset qAddProject=projectObj.createProject(form)/>
               </cfif>

               <!-- SIDE NAVIGATION END-->
               <!-- MAINPAGE CODE -->
               <div class="col-md-1 mainpage"></div>
               <div class="col-md-6 mainpage" >
                  
                  <h2><b>Add Project</b></h2><hr>
                  <form class="form-horizontal" id="frmCon" preservedata="true" method="post">
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_name">PROJECT NAME:</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control" id="project_name" placeholder="Project Name" name="project_name">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_clientname">CLIENT's Name:</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control" id="project_clientname" placeholder="CLIENT's Name" name="project_clientname">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_clientinfo">CLIENT INFO:</label>
                        <div class="col-sm-5">
                           <textarea name="project_clientinfo" rows="3" cols="57" ></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_ftpdetails">FTP DETAILS:</label>
                        <div class="col-sm-5">
                           <textarea name="project_ftpdetails" rows="3" cols="57"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_gitdetails">GIT DETAILS:</label>
                        <div class="col-sm-5">
                           <textarea name="project_gitdetails" rows="3" cols="57"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_dbdetails">DB DETAILS:</label>
                        <div class="col-sm-5">
                           <textarea name="project_dbdetails" rows="3" cols="57"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="project_url">URL:</label>
                        <div class="col-sm-5">
                           <textarea name="project_url" rows="3" cols="57"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                           <input type="submit" name="btn_AddProject" id="fld_submitLogin"class="btn btn-primary btn-lg active btnn" value="Submit" >
                        </div>
                     </div>
                  </form>
               </div>
               <!-- MAINPAGE CODE END -->	
            </div>
         </div>
         <div class="footer">
            <cfinclude template="includes/footer.cfm">
         </div>
      </body>
   </html>

   <cfelse>
   You are not allowed to access this page
</cfif>